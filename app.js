//gitlab repository: https://gitlab.com/santiblanco/midtermadb

//----------------------------
//variable n to determine number of events to run. change at will
var n = 12;
//----------------------------

var Promise = require('bluebird')
var adb = require('adbkit')
var client = adb.createClient()
var fs = require('fs');
var sleep = require('sleep');

var stream = fs.createWriteStream("report.html");

var installedAPK = 'com.example.apk_demo';

var bluetoothCommand = 'am start -a android.settings.BLUETOOTH_SETTINGS';

var addContact = "am start -a android.intent.action.INSERT -t vnd.android.cursor.dir/contact -e name 'Bo Lawson' -e phone 123456789";

var addContact2 = "am start -a android.intent.action.INSERT -t vnd.android.cursor.dir/contact -e name 'Bo Again' -e phone 123456789";


var i = 1;
var numIt = 1;

stream.once('open', async function () {
  var devices = await client.listDevices();
  var id = devices[0].id.toString();
  console.log(id);
  var isInstalled = await client.isInstalled(id,installedAPK);
  if(isInstalled){
    console.log('deleting apk before beginning')
    await client.uninstall(id,installedAPK);
  }
  ejecutarComandos(id);

});

async function ejecutarComandos(id) {
  while (i <= n) {
    stream.write('<h1>Iteration #' + numIt + '</h1>');

    //instalar apk 
    await client.install(id, './app-release.apk');

    await client.shell(id, 'monkey -p ' + installedAPK + ' ' + '-c android.intent.category.LAUNCHER 1');
    sleep.sleep(2);
    //ss de apk instalada
    var streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("insApp.png"));
    stream.write('<h2>Installed App by APK:</h2>\n');
    stream.write('<img src = "./insApp.png" width="200" height="420">');

    //home screen
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(5);

    //press first app
    await client.shell(id, 'input tap 142 2087');
    sleep.sleep(2);
    //ss first app
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("firstApp.png"));
    stream.write('<h2>Opening first app in home screen:</h2>\n');
    stream.write('<img src = "./firstApp.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //go home
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(2);

    //long press first app
    await client.shell(id, 'input touchscreen swipe 142 2087 142 2087 2000');
    sleep.sleep(2);
    //ss first long press
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("longPress1.png"));
    stream.write('<h2>First long press:</h2>\n');
    stream.write('<img src = "./longPress1.png" width="200" height="420">');


    //go home
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(2);

    //long press second app
    await client.shell(id, 'input touchscreen swipe 268 2065 268 2065 2000');
    sleep.sleep(2);
    //ss second long press
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("longPress2.png"));
    stream.write('<h2>Second long press:</h2>\n');
    stream.write('<img src = "./longPress2.png" width="200" height="420">');

    //go home
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(2);

    //long press third app
    await client.shell(id, 'input touchscreen swipe 546 2091 546 2091 2000');
    sleep.sleep(2);
    //ss third long press
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("longPress3.png"));
    stream.write('<h2>Third long press:</h2>\n');
    stream.write('<img src = "./longPress3.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //go back after two events
    await client.shell(id, 'input keyevent 4');
    sleep.sleep(2);

    //verify battery status
    var battery = await client.shell(id, 'dumpsys battery');
    var string = await adb.util.readAll(battery);
    stream.write('<h2>Verifed battery:</h2>\n');
    stream.write('<p>' + string.toString().trim() + '</p>');
    i++;
    if (i > n) break;

    //enable bt
    await client.shell(id, 'am start -a android.bluetooth.adapter.action.REQUEST_ENABLE');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 20');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 22');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 23');
    sleep.msleep(400);
    //ss bt
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("bt.png"));
    stream.write('<h2>Enabling bluetooth:</h2>\n');
    stream.write('<img src = "./bt.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //go back after two events
    await client.shell(id, 'input keyevent 4');
    sleep.sleep(2);

    //add a contact
    await client.shell(id, addContact);
    sleep.sleep(2);
    //ss adding a contact
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("contact1.png"));
    stream.write('<h2>Adding a contact:</h2>\n');
    stream.write('<img src = "./contact1.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //turning off bt to turn it on again later
    await client.shell(id, 'am start -a android.bluetooth.adapter.action.REQUEST_DISABLE');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 20');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 22');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 23');
    sleep.sleep(1);


    //go home
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(2);

    //press first app again
    await client.shell(id, 'input tap 142 2087');
    sleep.sleep(2);
    //ss first app
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("firstApp2.png"));
    stream.write('<h2>Opening first app in home screen again:</h2>\n');
    stream.write('<img src = "./firstApp2.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //go back after two events
    await client.shell(id, 'input keyevent 4');
    sleep.sleep(2);

    //reopening app to exit
    await client.shell(id, 'input tap 142 2087');
    sleep.sleep(2);
    //go home and exit app
    await client.shell(id, 'input keyevent 3');
    sleep.sleep(2);
    //ss exiting app
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("exitedApp.png"));
    stream.write('<h2>Exiting app:</h2>\n');
    stream.write('<img src = "./exitedApp.png" width="200" height="420">');
    i++;
    if (i > n) break;   
    
    //pulling screen resolution
    var screen = await client.shell(id, 'wm size');
    var screenString = await adb.util.readAll(screen);
    stream.write('<h2>Verifed screen resolution:</h2>\n');
    stream.write('<p>' + screenString.toString().trim() + '</p>');
    i++;
    if (i > n) break;  
    
    //go back after two events
    await client.shell(id, 'input keyevent 4');
    sleep.sleep(2);


    //enable bt again
    await client.shell(id, 'am start -a android.bluetooth.adapter.action.REQUEST_ENABLE');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 20');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 22');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 23');
    sleep.msleep(400);
    //ss bt
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("bt2.png"));
    stream.write('<h2>Enabling bluetooth again:</h2>\n');
    stream.write('<img src = "./bt.png" width="200" height="420">');
    i++;
    if (i > n) break;   
    
    //add a contact again
    await client.shell(id, addContact2);
    sleep.sleep(2);
    //ss adding a contact
    streamer = await client.screencap(id);
    await streamer.pipe(fs.createWriteStream("contact2.png"));
    stream.write('<h2>Adding a contact again:</h2>\n');
    stream.write('<img src = "./contact2.png" width="200" height="420">');
    i++;
    if (i > n) break;

    //go back after two events
    await client.shell(id, 'input keyevent 4');
    sleep.sleep(2);

    //turning off bt to turn it on again later
    await client.shell(id, 'am start -a android.bluetooth.adapter.action.REQUEST_DISABLE');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 20');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 22');
    sleep.sleep(1);
    await client.shell(id, 'input keyevent 23');
    sleep.sleep(1);

    //uninstalling the apk
    await client.uninstall(id, installedAPK);

    stream.write('<hr>')
    numIt++;
  }

}


