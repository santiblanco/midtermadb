**README**

**Santiago Blanco Cediel**

**201630630**

**Disclaimers**

*  The app is run on Node.js, therefore it should by installed on your computer
*  In order to use the node modules, your adb path must be a part of the PATH system variable of your computer
*  For the script to work correctly, the device must have the bluetooth off before executing
*  The app presses and long presses were done on a OnePlus 7 device and using the adb **input** command, which uses a pair of (x,y) parameters. Therefore, it is possible that if you use another device the tap will not register correctly due to differences in dimensions and icon sizes.
*  Because of the UI of the contacts app on the OnePlus device, it was not possible to save the contact on the contact list. However, the contact app will open and write basic info of a contact
*  In order to change the number of events N, you must open the app.js file. The first line of code of the file declares the variable *var n*. You may change it to your liking to decide the number of iterations of events.
*  During the iteration, you may see an uncalled event of the bluetooth turning off. This is done in order to turn it on again in later events
*  The report will be written in the file "report.html", in the same directory of the project. There, you can see the screenshots and information of events performed.

**Running the script**

From the terminal, cd into the project directory. Then, run the command **node app.js** to execute the script.

**Example**

You can see an example of the script running 12 events on a OnePlus 7 [here](https://youtu.be/82h7Da6KWu0)